#include <time.h>
#include <arduino.h>

#define FOLD_TIME 3500
#define TURN_OFF_DELAY FOLD_TIME + 500
#define LEFT_MIRROR_DIRECTION_PIN 0
#define LEFT_MIRROR_PWM_PIN 1
#define RIGHT_MIRROR_DIRECTION_PIN 2
#define RIGHT_MIRROR_PWM_PIN 3
#define ALIVE_PIN 4
#define IGNITION_PIN 5
#define FOLD_PIN 6
#define UNFOLD_PIN 7

enum Action { FOLD, UNFOLD, STOP };

bool isFolding = false;
bool isUnfolding = false;
time_t startFoldTime = 0;
time_t turnOffTime = TURN_OFF_DELAY;

void startFolding(Action actionType) {
  switch(actionType) {
      case FOLD:
        action(STOP);
        startFoldTime = millis();
        digitalWrite(LEFT_MIRROR_DIRECTION_PIN, LOW);
        digitalWrite(LEFT_MIRROR_PWM_PIN, HIGH);
        digitalWrite(RIGHT_MIRROR_DIRECTION_PIN, LOW);
        digitalWrite(RIGHT_MIRROR_PWM_PIN, HIGH);

        isFolding = true;
      break;

      case UNFOLD:
        action(STOP);
        startFoldTime = millis();
        digitalWrite(LEFT_MIRROR_DIRECTION_PIN, HIGH);
        digitalWrite(LEFT_MIRROR_PWM_PIN, LOW);
        digitalWrite(RIGHT_MIRROR_DIRECTION_PIN, HIGH);
        digitalWrite(RIGHT_MIRROR_PWM_PIN, LOW);

        isUnfolding = true;
      break;
    }
    turnOffTime = startFoldTime + TURN_OFF_DELAY;
}

void stopFolding() {
  digitalWrite(LEFT_MIRROR_DIRECTION_PIN, LOW);
  digitalWrite(LEFT_MIRROR_PWM_PIN, LOW);

  digitalWrite(RIGHT_MIRROR_DIRECTION_PIN, LOW);
  digitalWrite(RIGHT_MIRROR_PWM_PIN, LOW);

  // todo: remove me
  digitalWrite(LED_BUILTIN, LOW);
  isFolding = false;
  isUnfolding = false;
}

void action(Action actionType) {
  switch(actionType) {
    case FOLD:
      startFolding(FOLD);
      break;

    case UNFOLD:
      startFolding(UNFOLD);
      break;

    case STOP:
      stopFolding();
      break;
  }
}

void setup() {
  pinMode(ALIVE_PIN, OUTPUT);
  digitalWrite(ALIVE_PIN, LOW);

  pinMode(IGNITION_PIN, INPUT);
  pinMode(FOLD_PIN, INPUT_PULLUP);
  pinMode(UNFOLD_PIN, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(LEFT_MIRROR_DIRECTION_PIN, OUTPUT);
  pinMode(RIGHT_MIRROR_DIRECTION_PIN, OUTPUT);
}

void loop() {
  bool isActionEnded = (startFoldTime + FOLD_TIME) < millis();
  bool isTimeToSleep = millis() > turnOffTime;
  bool isIgnitionOn = digitalRead(IGNITION_PIN);

  if (isIgnitionOn || isTimeToSleep || isActionEnded) {
    action(STOP);
    digitalWrite(ALIVE_PIN, HIGH);
    return;
  }

  if (!digitalRead(FOLD_PIN) && !isFolding) {
    action(FOLD);
  }

  if (!digitalRead(UNFOLD_PIN) && !isUnfolding) {
    action(UNFOLD);
  }


//  if (isFolding) {
//    digitalWrite(LED_BUILTIN, HIGH);
//    delay(100);
//    digitalWrite(LED_BUILTIN, LOW);
//    delay(100);
//  }
//
//  if (isUnfolding) {
//    digitalWrite(LED_BUILTIN, HIGH);
//    delay(300);
//    digitalWrite(LED_BUILTIN, LOW);
//    delay(300);
//  }
}
